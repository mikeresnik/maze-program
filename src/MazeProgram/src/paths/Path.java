package paths;

import java.util.ArrayDeque;
import paths.coordinates.Coordinate;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public abstract class Path<COORD extends Coordinate, CELL> {

    public Stack<COORD> visited;
    public List<COORD> traversed;
    public Queue<COORD> order;
    public COORD start, current, end;
    public boolean isActive;
    protected static final int EMPTY = 0, TRAVERSED = 1, VISITED = 2, WALL = 3;
    protected CELL wallRepresentation;

    protected Path(COORD start, COORD end, CELL wallRepresentation) {
        this.visited = new Stack();
        this.traversed = new ArrayList();
        this.order = new ArrayDeque();
        this.traversed.add(start);
        this.start = start;
        this.current = start;
        this.end = end;
        this.isActive = false;
        this.wallRepresentation = wallRepresentation;
        this.initCollection();
    }

    protected abstract CELL get(COORD input);

    public final int getCoordValue(COORD input) {
        if (this.isWall(input)) {
            return WALL;
        } else if (this.visited.contains(input)) {
            return VISITED;
        } else if (this.traversed.contains(input)) {
            return TRAVERSED;
        }
        return EMPTY;
    }

    public final int[] getCoordValues(COORD... inputs) {
        int[] retArray = new int[inputs.length];
        for (int i = 0; i < inputs.length; i++) {
            retArray[i] = this.getCoordValue(inputs[i]);
        }
        return retArray;
    }

    protected final boolean isWall(COORD input) {
        return (this.wallRepresentation == this.get(input)) || this.wallRepresentation.equals(this.get(input));
    }

    protected abstract void initCollection();

    public abstract List<COORD> getVisitedRepresentation();

    public final boolean visited(COORD coord1) {
        return this.getVisitedRepresentation().contains(coord1);
    }

    public abstract List<COORD> getTraversedRepresentation();

    public final boolean traversed(COORD coord1) {
        return this.getTraversedRepresentation().contains(coord1);
    }

    public abstract COORD step() throws MazeException;

    public final int size() {
        return this.getVisitedRepresentation().size();
    }
}
