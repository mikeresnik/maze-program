package paths;

import java.util.ArrayList;
import java.util.List;
import paths.coordinates.Coordinate2d;

/**
 *
 * @author Mike Resnik
 * @since 2018-05-26
 */
public class DFS2d extends Path2d<Character> {

    private boolean backTrack = false;

    public DFS2d(Coordinate2d start, Coordinate2d end, char[][] searchSpace) {
        super(start, end, MazeGenerator.Util.charToCharacter(searchSpace), 'W');
        this.backTrack = false;
    }

    @Override
    protected void initCollection() {
        this.visited.push(start);
    }

    @Override
    public List<Coordinate2d> getVisitedRepresentation() {
        return new ArrayList(this.visited);
    }

    @Override
    public List<Coordinate2d> getTraversedRepresentation() {
        return this.traversed;
    }

    public static int findFirstMin(int[] values) {
        int min = Integer.MAX_VALUE;
        int pos = -1;
        for (int i = 0; i < values.length; i++) {
            if (values[i] < min) {
                min = values[i];
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public Coordinate2d step() {
        if (this.current.equals(end)) {
            this.isActive = false;
            return this.current;
        }
        Coordinate2d right = this.rightNode(), front = this.frontNode(), left = this.leftNode(), back = this.backNode();
        if (this.current.equals(start)) {
            boolean hasEmpty = false;
            int[] values = this.getCoordValues(right, front, left, back);
            for (int i = 0; i < values.length; i++) {
                if (values[i] == 0) {
                    hasEmpty = true;
                    break;
                }
            }
            if (hasEmpty == false) {
                throw MazeException.CANNOT_COMPLETE;
            }
        }

        Coordinate2d nextNode = null;
        if (this.backTrack) {
            Coordinate2d[] negative_rotation = new Coordinate2d[]{left, front, right};
            int[] negativeValues = this.getCoordValues(negative_rotation);
            int neg = findFirstMin(negativeValues);
            int minN = negativeValues[neg];
            nextNode = negative_rotation[neg];
            // pay attention to negative rotation and pick the lowest among the current
            switch (minN) {
                case EMPTY:
                    this.backTrack = false;
                    nextNode = this.step();
                    break;
                case TRAVERSED:
                    // test for 2
                    boolean hasVisited = false;
                    int visitIndex = -1;
                    for (int i = 0; i < negativeValues.length; i++) {
                        int curr = negativeValues[i];
                        if (curr == 2) {
                            hasVisited = true;
                            visitIndex = i;
                            break;
                        }
                    }
                    if (hasVisited == false) {
                        // Navigate to nextNode
                        visited.push(nextNode);
                        break;
                    } else {
                        neg = visitIndex;
                        minN = negativeValues[neg];
                        nextNode = negative_rotation[neg];
                    }
                case VISITED:
                    visited.remove(current);
                    break;
                case WALL:
                    throw MazeException.CANNOT_COMPLETE;
                default:
                    break;
            }
        } else {
            Coordinate2d[] positive_rotation = new Coordinate2d[]{right, front, left};
            int[] positiveValues = this.getCoordValues(positive_rotation);
            int pos = findFirstMin(positiveValues);
            int minP = positiveValues[pos];
            nextNode = positive_rotation[pos];
            switch (minP) {
                case EMPTY: // IS EMPTY
                    visited.push(nextNode);
                    traversed.add(nextNode);
                    break;
                case TRAVERSED: // IS TRAVERSED
                    visited.push(nextNode);
                    break;
                case VISITED: // IS VISITED
                case WALL:
                    this.backTrack = true;
                    this.visited.remove(this.current);
                    this.rotateRight();
                    this.rotateRight();
                    nextNode = this.step();
                default:
            }
        }
        if (nextNode.equals(left)) {
            this.rotateLeft();
        } else if (nextNode.equals(right)) {
            this.rotateRight();
        }
        this.current = nextNode;
        return nextNode;
    }

}
