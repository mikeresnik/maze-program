package paths;

import java.util.ArrayList;
import java.util.List;
import static paths.Path2d.Direction.*;
import paths.coordinates.Coordinate2d;

public abstract class Path2d<CELL> extends Path<Coordinate2d, CELL> {

    public final CELL[][] searchSpace;
    public Direction direction;

    public Path2d(Coordinate2d start, Coordinate2d end, CELL[][] searchSpace, CELL wallRepresentation) {
        super(start, end, wallRepresentation);
        this.searchSpace = searchSpace;
        this.direction = DOWN;
        // System.out.println("starting direction:" + this.direction);
    }

    public enum Direction {
        UP(), DOWN(), LEFT(), RIGHT();
    }

    @Override
    public final CELL get(Coordinate2d node) {
        return this.searchSpace[node.x][node.y];
    }

    public static Direction directionOpposite(Direction input) {
        Direction retDirection = null;
        switch (input) {
            case UP:
                retDirection = DOWN;
                break;
            case DOWN:
                retDirection = UP;
                break;
            case LEFT:
                retDirection = RIGHT;
                break;
            case RIGHT:
                retDirection = LEFT;
                break;
            default:
                retDirection = null;
        }
        return retDirection;
    }

    protected final void rotateRight() {
        switch (this.direction) {
            case UP:
                this.direction = RIGHT;
                break;
            case DOWN:
                this.direction = LEFT;
                break;
            case LEFT:
                this.direction = UP;
                break;
            case RIGHT:
                this.direction = DOWN;
                break;
            default:
                this.direction = null;
        }
        // System.out.println("newDirection:" + this.direction);
    }

    protected final void rotateLeft() {
        switch (this.direction) {
            case UP:
                this.direction = LEFT;
                break;
            case DOWN:
                this.direction = RIGHT;
                break;
            case LEFT:
                this.direction = DOWN;
                break;
            case RIGHT:
                this.direction = UP;
                break;
            default:
                this.direction = null;
        }
        // System.out.println("newDirection:" + this.direction);
    }

    protected final Coordinate2d leftNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_left;
            case DOWN:
                return map_right;
            case LEFT:
                return map_down;
            case RIGHT:
                return map_up;
            default:
                return null;
        }
    }

    protected final Coordinate2d rightNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_right;
            case DOWN:
                return map_left;
            case LEFT:
                return map_up;
            case RIGHT:
                return map_down;
            default:
                return null;
        }
    }

    protected final Coordinate2d frontNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_up;
            case DOWN:
                return map_down;
            case LEFT:
                return map_left;
            case RIGHT:
                return map_right;
            default:
                return null;
        }
    }

    public Coordinate2d[] findNeighbors() {
        ArrayList<Coordinate2d> neighbors = new ArrayList<>();
        if (!isWall(this.leftNode())) {
            neighbors.add(this.leftNode());
        }
        if (!isWall(this.frontNode())) {
            neighbors.add(this.frontNode());
        }
        if (!isWall(this.rightNode())) {
            neighbors.add(this.rightNode());
        }
        if (!isWall(this.backNode())) {
            neighbors.add(this.backNode());
        }
        return neighbors.toArray(new Coordinate2d[neighbors.size()]);
    }

    public final Coordinate2d backNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_down;
            case DOWN:
                return map_up;
            case LEFT:
                return map_right;
            case RIGHT:
                return map_left;
            default:
                return null;
        }
    }


}
