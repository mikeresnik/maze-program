package paths;

public class MazeException extends RuntimeException {

    public static final MazeException CANNOT_COMPLETE = new MazeException("Maze cannot be completed.");

    protected MazeException() {
        super(); // Redundant
    }

    public MazeException(String cause) {
        super(cause);
    }

}
