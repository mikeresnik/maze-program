package paths;

import java.util.List;
import paths.coordinates.Coordinate2d;


public class Genetic2d extends Path2d<Character>{

    private String grammar;
    private int grammar_index;
    
    public Genetic2d(Coordinate2d start, Coordinate2d end, char[][] searchSpace) {
        this(start, end, MazeGenerator.Util.charToCharacter(searchSpace));
    }

    public Genetic2d(Coordinate2d start, Coordinate2d end, Character[][] searchSpace) {
        super(start, end, searchSpace, 'W');
    }

    @Override
    protected void initCollection() {
    
    }

    @Override
    public List<Coordinate2d> getVisitedRepresentation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Coordinate2d> getTraversedRepresentation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Coordinate2d step() throws MazeException {
        grammar_index++;
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Coordinate2d nextFromGrammar(){
        char decision = this.grammar.charAt(grammar_index);
        // decision stuff
        return null; // nextCoord
    }
}
