package paths;

import java.util.ArrayList;
import java.util.List;
import paths.coordinates.Coordinate2d;

/**
 *
 * @author Hadi Soufi
 * @since 2018-06-01
 */
public class BFS2d extends Path2d<Character> {

    private int[][] intRepresentation;

    public BFS2d(Coordinate2d start, Coordinate2d end, char[][] searchSpace) {
        super(start, end, MazeGenerator.Util.charToCharacter(searchSpace), 'W');
        this.initINT2d();
    }

    @Override
    protected void initCollection() {
        this.order.add(start);
    }

    private void initINT2d() {
        this.intRepresentation = new int[this.searchSpace.length][this.searchSpace[0].length];
        for (int i = 0; i < this.intRepresentation.length; i++) {
            for (int j = 0; j < this.intRepresentation[0].length; j++) {
                this.intRepresentation[i][j] = (this.searchSpace[i][j] == 'C' ? EMPTY : WALL);
            }
        }
    }

    @Override
    public List<Coordinate2d> getVisitedRepresentation() {
        return this.traversed;
    }

    @Override
    public List<Coordinate2d> getTraversedRepresentation() {
        return new ArrayList(this.order);
    }

    @Override
    public Coordinate2d step() {
        if (this.current.equals(end)) {
            this.isActive = false;
            if (this.visited.contains(this.current) == false) {
                this.visited.add(this.current);
            }
            return this.current;
        }

        Coordinate2d[] neighbors = findNeighbors();
        for (Coordinate2d neighbor : neighbors) {
            if (intRepresentation[neighbor.x][neighbor.y] == EMPTY) {
                this.order.add(neighbor);
                intRepresentation[neighbor.x][neighbor.y] = VISITED;
            }
        }

        this.traversed.add(this.current);
        intRepresentation[current.x][current.y] = TRAVERSED;
        if (this.order.isEmpty()) {
            throw new MazeException("Maze cannot be completed.");
        }
        this.current = this.order.remove();

        return this.current;
    }

}
