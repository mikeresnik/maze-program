package paths.coordinates;

import java.util.ArrayList;
import paths.dimensions.Dimension;
import paths.dimensions.Dimension4d;

public class Coordinate4d extends Coordinate<Coordinate4d, Dimension4d> {

    public final int x, y, z, w;

    public Coordinate4d(int x, int y, int z, int w) {
        super(x, y, z, w);
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    @Override
    public Coordinate4d[] getNeighbors() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer[] getNeighborValues(Dimension4d dimension, ArrayList<Integer> searchSpace) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
