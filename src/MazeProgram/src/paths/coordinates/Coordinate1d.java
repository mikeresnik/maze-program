package paths.coordinates;

import java.util.ArrayList;
import paths.dimensions.Dimension;
import paths.dimensions.Dimension1d;

public class Coordinate1d extends Coordinate<Coordinate1d, Dimension1d> {

    public final int x;

    public Coordinate1d(int x) {
        super(x);
        this.x = x;
    }

    @Override
    public Coordinate1d[] getNeighbors() {
        Coordinate1d[] retArray = new Coordinate1d[]{new Coordinate1d(x - 1), new Coordinate1d(x + 1)};
        return retArray;
    }

    @Override
    public Integer[] getNeighborValues(Dimension1d dimension, ArrayList<Integer> searchSpace) {
        Dimension.justifyDimensions(dimension, searchSpace);
        Integer[] retArray = new Integer[]{null, null};
        Coordinate1d[] neighbors = this.getNeighbors();
        for (int x_index = 0; x_index < dimension.l; x_index++) {
            Integer curr = searchSpace.get(x_index);
            for (int i = 0; i < neighbors.length; i++) {
                if (x_index == neighbors[i].x) {
                    retArray[i] = curr;
                    break;
                }
            }
        }
        return retArray;
    }

}
