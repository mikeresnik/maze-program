package paths.coordinates;

import java.util.ArrayList;
import java.util.Arrays;
import paths.dimensions.Dimension;

public abstract class Coordinate<CHILDTYPE extends Coordinate, DIM extends Dimension> {

    protected final int[] allDims;

    protected Coordinate(int... allDims) {
        this.allDims = allDims;
    }

    public final int[] getAllDims() {
        return allDims;
    }

    public final int dimension() {
        return allDims.length;
    }

    public abstract CHILDTYPE[] getNeighbors();

    public abstract Integer[] getNeighborValues(DIM dimension, ArrayList<Integer> searchSpace);

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate<?, ?> other = (Coordinate<?, ?>) obj;
        if (!Arrays.equals(this.allDims, other.allDims)) {
            return false;
        }
        return true;
    }

}
