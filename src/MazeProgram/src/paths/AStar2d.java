package paths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import paths.coordinates.Coordinate2d;
import static paths.coordinates.Coordinate2d.manhattanDistance;

public class AStar2d extends Path2d<Character> {

    private List<Coordinate2d> open, closed;
    private Map<Coordinate2d, int[]> fitnessMap;
    private static final int G_INDEX = 0, H_INDEX = 1, F_INDEX = 2;

    public AStar2d(Coordinate2d start, Coordinate2d end, char[][] searchSpace) {
        this(start, end, MazeGenerator.Util.charToCharacter(searchSpace));
    }

    public AStar2d(Coordinate2d start, Coordinate2d end, Character[][] searchSpace) {
        super(start, end, searchSpace, 'W');
    }

    @Override
    protected void initCollection() {
        this.open = new ArrayList();
        this.open.add(this.start);
        this.closed = new ArrayList();
        this.fitnessMap = new HashMap();
        final int g_init = gCost(start);
        this.fitnessMap.put(this.start, new int[]{0, g_init, 0 + g_init});
    }

    private Coordinate2d findNextNeighbor() {
        List<Coordinate2d> fValues = lowestOpenFValues();
        List<Coordinate2d> hValues = lowestHValues(fValues);
        if (hValues.isEmpty()) {
            throw MazeException.CANNOT_COMPLETE;
        }
        return hValues.get((int) (Math.random() * hValues.toArray().length));
    }

    @Override
    public List<Coordinate2d> getVisitedRepresentation() {
        return this.closed;
    }

    @Override
    public List<Coordinate2d> getTraversedRepresentation() {
        return this.open;
    }

    private int gCost(Coordinate2d node) {
        return manhattanDistance(start, node);
    }

    private int hCost(Coordinate2d node) {
        return manhattanDistance(node, end);
    }

    private List<Coordinate2d> lowestOpenFValues() {
        List<Coordinate2d> retList = new ArrayList();
        int lowestF = Integer.MAX_VALUE;
        for (Coordinate2d node : this.open) {
            final int[] fitness = this.fitnessMap.get(node);
            if (fitness[F_INDEX] < lowestF) {
                lowestF = fitness[F_INDEX];
                retList.clear();
                retList.add(node);
            }
            if (fitness[F_INDEX] == lowestF) {
                retList.add(node);
            }
        }
        return retList;
    }

    private List<Coordinate2d> lowestHValues(List<Coordinate2d> inputList) {
        List<Coordinate2d> retList = new ArrayList();
        int lowestH = Integer.MAX_VALUE;
        for (Coordinate2d node : inputList) {
            final int[] fitness = this.fitnessMap.get(node);
            if (fitness[H_INDEX] < lowestH) {
                lowestH = fitness[H_INDEX];
                retList.clear();
                retList.add(node);
            }
            if (fitness[H_INDEX] == lowestH) {
                retList.add(node);
            }
        }
        return retList;
    }

    private int[] scoreNeighbor(Coordinate2d neighbor) {
        int[] oldFitness = fitnessMap.getOrDefault(neighbor, new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE});
        int[] currentFitness = fitnessMap.get(current);
        int[] retArray = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE};
        int expected_G_VALUE = currentFitness[G_INDEX] + 1;
        if (oldFitness[G_INDEX] > expected_G_VALUE) {
            retArray[G_INDEX] = expected_G_VALUE;
            retArray[H_INDEX] = hCost(neighbor);
            retArray[F_INDEX] = retArray[G_INDEX] + retArray[H_INDEX];
        } else {
            retArray = oldFitness;
        }
        return retArray;
    }

    @Override
    public Coordinate2d step() {
        if (this.end.equals(this.current)) {
            this.isActive = false;
            return this.current;
        }
        this.open.remove(current);
        this.closed.add(current);
        Coordinate2d[] neighbors = this.findNeighbors();
        for (Coordinate2d neighbor : neighbors) {
            if (this.closed.contains(neighbor)) {
                continue;
            }
            // score neighbors (update from previous)
            int[] newScore = this.scoreNeighbor(neighbor);
            this.fitnessMap.remove(neighbor);
            this.fitnessMap.put(neighbor, newScore);
            // add neighbors to open set
            if (this.open.contains(neighbor) == false) {
                this.open.add(neighbor);
            }
        }
        this.current = findNextNeighbor();
        return this.current;
    }

}
