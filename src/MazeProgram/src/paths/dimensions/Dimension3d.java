package paths.dimensions;

import paths.coordinates.Coordinate3d;

public class Dimension3d extends Dimension<Coordinate3d> {

    public final int l, w, h;

    public Dimension3d(int l, int w, int h) {
        super(l, w, h);
        this.l = l;
        this.w = w;
        this.h = h;
    }

}
