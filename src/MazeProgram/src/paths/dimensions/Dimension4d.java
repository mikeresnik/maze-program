package paths.dimensions;

import paths.coordinates.Coordinate4d;

public class Dimension4d extends Dimension<Coordinate4d> {

    public final int l, w, h, d;

    public Dimension4d(int l, int w, int h, int d) {
        super(l, w, h, d);
        this.l = l;
        this.w = w;
        this.h = h;
        this.d = d;
    }

}
