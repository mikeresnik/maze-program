package paths.dimensions;

import paths.coordinates.Coordinate2d;

public class Dimension2d extends Dimension<Coordinate2d> {

    public final int l, w;

    public Dimension2d(int l, int w) {
        super(l, w);
        this.l = l;
        this.w = w;
    }

}
