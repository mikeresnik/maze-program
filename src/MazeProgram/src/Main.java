
import paths.MazeGenerator;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import paths.*;
import paths.coordinates.Coordinate2d;
import util.colors;

public class Main extends Application {

    public Scene scene;
    public Stage stage;
    private SceneContainer controller;

    private char[][] maze_primary;
    private Character[][] maze_objective;
    private static int GRID_SIZE = 13;
    private Path2d<Character> path;
    private PATHNAME selectedPath;

    public enum PATHNAME {
        NONE, DFS, BFS, ASTAR, GENETIC, NEURAL;
    }

    private static final class SceneContainer {

        public Scene scene;
        public Stage stage;

        private GridPane mazePane;
        private ScrollPane box1, box2;
        private ToggleGroup toggleGroup;
        private RadioButton dfsRadio, bfsRadio, aStarRadio, genRadio, nnRadio;
        private Button randButton;
        private Button plusTen, minusTen;
        private Button backstep, pause, play, step;
        private GridPane subPane;
        private TextField mazeTextField;

        public SceneContainer(Scene scene, Stage stage) {
            this.scene = scene;
            this.stage = stage;
            this.mazePane = (GridPane) scene.lookup("#mazePane");
        }

    }

    private void initComponents() {
        // Justify odd maze size values.
        this.refresh();
    }

    public void initEvents() {
        ChangeListener<Number> c
                = (observer, oldval, newval) -> {
                    this.resizeMaze();
                };
        this.controller.mazePane.widthProperty().addListener(c);
        this.controller.mazePane.heightProperty().addListener(c);
        stage.setOnCloseRequest(
                (t) -> {
                    Platform.exit();
                    System.exit(0);
                }
        );
        stage.maximizedProperty().addListener(
                (observer, oldval, newval) -> {
                    Platform.runLater(
                            () -> {
                                resizeMaze();
                            }
                    );
                }
        );
        Platform.runLater(
                () -> {
                    this.controller.toggleGroup = new ToggleGroup();
                    this.controller.dfsRadio = (RadioButton) this.scene.lookup("#dfsRadio");
                    this.controller.dfsRadio.setToggleGroup(controller.toggleGroup);
                    this.controller.dfsRadio.setOnAction(
                            (e) -> {
                                this.selectedPath = PATHNAME.DFS;
                                this.switchPath();
                                this.whiten();
                            }
                    );

                    this.controller.bfsRadio = (RadioButton) this.scene.lookup("#bfsRadio");
                    this.controller.bfsRadio.setToggleGroup(controller.toggleGroup);
                    this.controller.bfsRadio.setOnAction(
                            (e) -> {
                                this.selectedPath = PATHNAME.BFS;
                                this.switchPath();
                                this.whiten();
                            }
                    );

                    this.controller.aStarRadio = (RadioButton) this.scene.lookup("#aStarRadio");
                    this.controller.aStarRadio.setToggleGroup(controller.toggleGroup);
                    this.controller.aStarRadio.setOnAction(
                            (e) -> {
                                this.selectedPath = PATHNAME.ASTAR;
                                this.switchPath();
                                this.whiten();
                            }
                    );

                    this.controller.genRadio = (RadioButton) this.scene.lookup("#genRadio");
                    this.controller.genRadio.setToggleGroup(controller.toggleGroup);
                    this.controller.genRadio.setOnAction(
                            (e) -> {
                                this.selectedPath = PATHNAME.GENETIC;
                                this.switchPath();
                                this.whiten();
                            }
                    );

                    this.controller.nnRadio = (RadioButton) this.scene.lookup("#nnRadio");
                    this.controller.nnRadio.setToggleGroup(controller.toggleGroup);
                    this.controller.nnRadio.setOnAction(
                            (e) -> {
                                this.selectedPath = PATHNAME.NEURAL;
                                this.switchPath();
                                this.whiten();
                            }
                    );

                    this.controller.randButton = (Button) this.scene.lookup("#randButton");
                    this.controller.randButton.setOnAction(
                            (e) -> {
                                refresh();
                            }
                    );
                    this.controller.mazeTextField = (TextField) this.scene.lookup("#mazeTextField");
                    this.controller.mazeTextField.setOnAction(
                            (e) -> {
                                String val = controller.mazeTextField.getText();
                                if (val.matches("[0-9]*")) {
                                    int num_val = Integer.parseInt(val);
                                    GRID_SIZE = num_val;
                                } else {
                                    // THROW ERROR
                                    throw new IllegalArgumentException("Value of text field must be an integer");
                                }
                                initComponents();
                            }
                    );
                    this.controller.plusTen = (Button) this.scene.lookup("#plusTen");
                    this.controller.plusTen.setOnAction(
                            (e) -> {
                                GRID_SIZE += 10;
                                this.controller.mazeTextField.setText("" + GRID_SIZE);
                                initComponents();
                            }
                    );
                    this.controller.minusTen = (Button) this.scene.lookup("#minusTen");
                    this.controller.minusTen.setOnAction(
                            (e) -> {
                                GRID_SIZE -= 10;
                                GRID_SIZE = Math.max(GRID_SIZE, 10);
                                this.controller.mazeTextField.setText("" + GRID_SIZE);
                                initComponents();
                            }
                    );
                    this.controller.step = (Button) this.scene.lookup("#stepButton");
                    this.controller.step.setOnAction(
                            (e) -> {
                                step();
                            }
                    );
                    this.controller.play = (Button) this.scene.lookup("#playButton");

                    this.controller.play.setOnAction(
                            (e) -> {
                                if (this.path == null) {
                                    return;
                                }
                                this.path.isActive = true;
                            }
                    );

                    this.controller.pause = (Button) this.scene.lookup("#pauseButton");
                    this.controller.pause.setOnAction(
                            (e) -> {
                                if (this.path == null) {
                                    return;
                                }
                                this.path.isActive = false;
                                System.out.println("paused");
                            }
                    );
                }
        );

    }

    private GridPane generateMaze() {
        double perc = 100.0 / GRID_SIZE;
        GridPane newPane = new GridPane();
        for (int r_index = 0; r_index < GRID_SIZE; r_index++) {
            RowConstraints row = new RowConstraints();
            row.setPercentHeight(perc);
            newPane.getRowConstraints().add(row);
        }
        for (int c_index = 0; c_index < GRID_SIZE; c_index++) {
            ColumnConstraints col = new ColumnConstraints();
            col.setPercentWidth(perc);
            newPane.getColumnConstraints().add(col);
        }
        this.maze_primary = MazeGenerator.recursiveBacktracker(GRID_SIZE - 2);
        this.maze_objective = MazeGenerator.Util.charToCharacter(maze_primary);
        for (int i = 0; i < GRID_SIZE; i++) {
            for (int j = 0; j < GRID_SIZE; j++) {
                GridPane rect = new GridPane();
                Color curr_color;
                char curr_state = maze_primary[i][j];
                switch (curr_state) {
                    case 'C':
                        curr_color = Color.WHITE;
                        break;
                    case 'W':
                    default:
                        curr_color = Color.BLACK;
                }
                rect.setStyle("-fx-background-color: #" + colors.colorToHex(curr_color));
                newPane.add(rect, i, j);
                GridPane.setHalignment(rect, HPos.CENTER);
            }
        }
        newPane.gridLinesVisibleProperty().set(true);
        GridPane.setHgrow(newPane, Priority.NEVER);
        GridPane.setHalignment(newPane, HPos.CENTER);
        return newPane;
    }

    public GridPane getGridByRowCol(final int row, final int column) {
        Node result = null;
        ObservableList<Node> childrens = this.controller.subPane.getChildren();

        for (Node node : childrens) {
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }
        return (GridPane) result;
    }

    private void refresh() {
        this.controller.mazePane.getChildren().clear();
        this.controller.subPane = generateMaze();
        this.controller.mazePane.getChildren().add(controller.subPane);
        this.resizeMaze();
        this.switchPath();
    }

    private void resizeMaze() {
        Bounds paneBounds = controller.mazePane.localToScene(controller.mazePane.getBoundsInLocal());
        double maxPossible = Math.min(paneBounds.getWidth(), paneBounds.getHeight());
        controller.subPane.setMaxSize(maxPossible, maxPossible);
        double dim = maxPossible / GRID_SIZE;
        ObservableList<Node> children = controller.subPane.getChildren();
    }

    private void shadeCoordinate(Coordinate2d node) {
        if (this.path.visited(node)) {
            GridPane nextGrid = this.getGridByRowCol(node.y, node.x);
            String newStyle = "-fx-background-color: #FF5686";
            if (nextGrid.getStyle().equals(newStyle) == false) {
                nextGrid.setStyle(newStyle);
            }
        } else if (this.path.traversed(node)) {
            GridPane nextGrid = this.getGridByRowCol(node.y, node.x);
            String newStyle = "-fx-background-color: #597FFF";
            if (nextGrid.getStyle().equals(newStyle) == false) {
                nextGrid.setStyle(newStyle);
            }
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        this.stage = stage;
        this.scene = new Scene(root);
        this.controller = new SceneContainer(scene, stage);
        this.selectedPath = PATHNAME.NONE;
        // ABSTRACT to Controller subclass
        this.initComponents();
        this.initEvents();
        this.stage.setScene(scene);
        this.stage.show();
        new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                step();
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void step() {
        if (path == null || path.isActive == false) {
            return;
        }
        Coordinate2d oldNode = path.current;
        Coordinate2d newNode = null;
        try {
            newNode = path.step();
        } catch (MazeException me) {
            System.out.println("Maze Cannot be Completed");
            path.isActive = false;
            return;
        }

        // GridPane nextGrid = this.getGridByRowCol(nextNode.y, nextNode.x);
        for (Coordinate2d trav : this.path.getTraversedRepresentation()) {
            shadeCoordinate(trav);
        }
        for (Coordinate2d vis : this.path.getVisitedRepresentation()) {
            shadeCoordinate(vis);
        }

    }

    private void switchPath() {
        Coordinate2d start = MazeGenerator.Util.findFirst(this.maze_objective, 'C');
        Coordinate2d end = MazeGenerator.Util.findLast(this.maze_objective, 'C');
        // System.out.printf("start:%s \nend:%s\n", start, end);
        switch (this.selectedPath) {
            case DFS:
                this.path = new DFS2d(start, end, this.maze_primary);
                break;
            case BFS:
                this.path = new BFS2d(start, end, this.maze_primary);
                break;
            case ASTAR:
                this.path = new AStar2d(start, end, this.maze_primary);
                break;
            case GENETIC:
                this.path = null;
                break;
            case NEURAL:
                this.path = null;
                break;
            default:
                this.path = null;
        }
    }

    private void whiten() {
        for (int i = 0; i < GRID_SIZE; i++) {
            for (int j = 0; j < GRID_SIZE; j++) {
                if (maze_primary[i][j] == 'C') {
                    this.getGridByRowCol(j, i).setStyle("-fx-background-color: #" + colors.colorToHex(Color.WHITE));
                }
            }
        }
    }

}
