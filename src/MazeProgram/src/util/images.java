package util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;

/**
 *
 * @author Mike
 */
public final class images {

    private static final boolean DEBUG = true;
    private static final int BLACK = 0x00000000, WHITE = 0xffffffff;
    private static final byte[] BLACK_b = new byte[]{BLACK, BLACK, BLACK}, WHITE_b = new byte[]{(byte) WHITE, (byte) WHITE, (byte) WHITE};
    private static final double RPERC = 0.30, GPERC = 0.59, BPERC = 0.11;

    private images() {

    }

    /**
     * Structuring element
     */
    public static final class strel {

        private final int[][] int_representation;
        private final int[][][] vec_representation;
        private final int center_x, center_y;

        private strel(int[][] int_representation, int center_x, int center_y) {
            this.int_representation = int_representation;
            this.center_x = center_x;
            this.center_y = center_y;
            System.out.printf("x:%s\ty:%s\n", center_x, center_y);
            this.vec_representation = new int[this.int_representation.length][this.int_representation[0].length][2];
            for (int ROW = 0; ROW < this.int_representation.length; ROW++) {
                for (int COL = 0; COL < this.int_representation[0].length; COL++) {
                    this.vec_representation[ROW][COL] = (this.int_representation[ROW][COL] == 1 ? new int[]{COL - center_x, ROW - center_x} : null);
                }
            }
        }

        public void print() {
            for (int i = 0; i < this.int_representation.length; i++) {
                System.out.println(Arrays.toString(this.int_representation[i]));
            }
        }

        public void printVec() {
            for (int ROW = 0; ROW < this.vec_representation.length; ROW++) {
                System.out.println(Arrays.deepToString(this.vec_representation[ROW]));
            }

        }

        public int[][][] apply_vec_representation(int[] coord) {
            int[][][] retArray = new int[this.vec_representation.length][this.vec_representation[0].length][];
            for (int ROW = 0; ROW < this.vec_representation.length; ROW++) {
                for (int COL = 0; COL < this.vec_representation[0].length; COL++) {
                    int[] currVec = this.vec_representation[ROW][COL];
                    retArray[ROW][COL] = (currVec != null ? new int[]{coord[0] + currVec[0], coord[1] + currVec[1]} : null);
                }
            }
            return retArray;
        }

        public static strel square(int size) {
            size = (size % 2 == 0 ? size + 1 : size);
            int[][] temp_rep = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    temp_rep[i][j] = 1;
                }
            }
            return new strel(temp_rep, size / 2, size / 2);
        }

        public static strel diamond(int size) {
            size = (size % 2 == 0 ? size + 1 : size);
            double[] center = new double[]{size / 2, size / 2};
            int[][] temp_rep = new int[size][size];
            for (int row = 0; row < size; row++) {
                for (int col = 0; col < size; col++) {
                    double dist = Math.abs(row - center[1]) + Math.abs(col - center[0]);
                    temp_rep[row][col] = (dist > size / 2 ? 0 : 1);
                }
            }
            return new strel(temp_rep, size / 2, size / 2);
        }

        public static strel circle(int radius) {
            int[][] temp_rep = new int[2 * radius + 1][2 * radius + 1];
            double[] center = new double[]{(2 * radius + 1) / 2, (2 * radius + 1) / 2};
            for (int row = 0; row < temp_rep.length; row++) {
                for (int col = 0; col < temp_rep[0].length; col++) {
                    double dist = Math.sqrt(Math.pow(center[1] - row, 2) + Math.pow(center[0] - col, 2));
                    temp_rep[row][col] = (dist > radius ? 0 : 1);
                }
            }
            return new strel(temp_rep, (2 * radius + 1) / 2, (2 * radius + 1) / 2);
        }

        public static strel rect(int height, int width, int c_x, int c_y) {
            int[][] temp_rep = new int[height][width];
            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    temp_rep[row][col] = 1;
                }
            }
            return new strel(temp_rep, c_x, c_y);
        }

    }

    public static byte[][][] erodeBinary(byte[][][] binaryImage, strel B) {
        byte[][][] retImage = new byte[binaryImage.length][binaryImage[0].length][];
        for (int image_ROW = 0; image_ROW < binaryImage.length; image_ROW++) {
            for (int image_COL = 0; image_COL < binaryImage[0].length; image_COL++) {
                int[][][] coordCheck = B.apply_vec_representation(new int[]{image_COL, image_ROW});

                boolean forAll = true;
                for (int c_ROW = 0; forAll && c_ROW < coordCheck.length; c_ROW++) {
                    for (int c_COL = 0; forAll && c_COL < coordCheck[0].length; c_COL++) {
                        try {
                            int[] coord = coordCheck[c_ROW][c_COL];
                            if (coord == null) {
                                continue;
                            }
                            if (Arrays.equals(binaryImage[coord[1]][coord[0]], BLACK_b) == false) {
                                forAll = false;
                                continue;
                            }
                        } catch (Exception e) {
                            continue;
                        }

                    }
                }
                retImage[image_ROW][image_COL] = (forAll ? BLACK_b : WHITE_b);
            }
        }
        return retImage;
    }

    public static byte[][][] dilateBinary(byte[][][] binaryImage, strel B) {
        byte[][][] retImage = new byte[binaryImage.length][binaryImage[0].length][];
        for (int image_ROW = 0; image_ROW < binaryImage.length; image_ROW++) {
            for (int image_COL = 0; image_COL < binaryImage[0].length; image_COL++) {
                retImage[image_ROW][image_COL] = WHITE_b;
                if (Arrays.equals(binaryImage[image_ROW][image_COL], BLACK_b)) {
                    int[][][] coordCheck = B.apply_vec_representation(new int[]{image_COL, image_ROW});
                    for (int c_ROW = 0; c_ROW < coordCheck.length; c_ROW++) {
                        for (int c_COL = 0; c_COL < coordCheck[0].length; c_COL++) {
                            try {
                                int[] coord = coordCheck[c_ROW][c_COL];
                                if (coord == null) {
                                    continue;
                                }
                                retImage[coord[1]][coord[0]] = BLACK_b;
                            } catch (Exception e) {
                                continue;
                            }
                        }
                    }
                }

            }
        }
        return retImage;
    }

    public static byte[][][] openBinary(byte[][][] binaryImage, strel B) {
        return dilateBinary(erodeBinary(binaryImage, B), B);
    }

    public static byte[][][] closeBinary(byte[][][] binaryImage, strel B) {
        return erodeBinary(dilateBinary(binaryImage, B), B);
    }

    public static List<int[]> binaryToList(byte[][][] binaryImage) {
        List<int[]> retList = new ArrayList();
        for (int ROW = 0; ROW < binaryImage.length; ROW++) {
            for (int COL = 0; COL < binaryImage[0].length; COL++) {
                if (Arrays.equals(binaryImage[ROW][COL], new byte[]{BLACK, BLACK, BLACK})) {
                    retList.add(new int[]{COL, ROW});
                }
            }
        }
        return retList;
    }

    public static byte[][][] listToBinary(List<int[]> coordImage) {
        int max_height = 0, max_width = 0;
        for (int[] coord : coordImage) {
            max_width = Math.max(coord[0], max_width);
            max_height = Math.max(coord[1], max_height);
        }
        System.out.printf("mw:%s\tmh:%s\n", max_width, max_height);
        byte[][][] retImage = new byte[max_height + 1][max_width + 1][];
        for (int ROW = 0; ROW < max_height + 1; ROW++) {
            for (int COL = 0; COL < max_width + 1; COL++) {
                retImage[ROW][COL] = WHITE_b;
            }
        }
        for (int[] coord : coordImage) {
            retImage[coord[1]][coord[0]] = BLACK_b;
        }
        return retImage;
    }

    public static byte[][][] generateSlopeField(byte[][][] original) {
        byte[][][] new_image = new byte[2 * original.length - 1][2 * original[0].length - 1][original[0][0].length];
        for (int ROW_INDEX = 0; ROW_INDEX < original.length; ROW_INDEX++) {
            for (int COL_INDEX = 0; COL_INDEX < original[0].length; COL_INDEX++) {
                byte[] current = original[ROW_INDEX][COL_INDEX];
                // new_image[2*ROW_INDEX][2*COL_INDEX] = current;
                // try right
                try {
                    byte[] right = original[ROW_INDEX][COL_INDEX + 1];
                    byte[] delta = arrays.deltaBytes(current, right);
                    new_image[2 * ROW_INDEX][2 * COL_INDEX + 1] = delta;
                } catch (ArrayIndexOutOfBoundsException aioobe) {
                }
                //try down
                try {
                    byte[] down = original[ROW_INDEX + 1][COL_INDEX];
                    byte[] delta = arrays.deltaBytes(current, down);
                    new_image[2 * ROW_INDEX + 1][2 * COL_INDEX] = delta;
                } catch (ArrayIndexOutOfBoundsException aioobe) {

                }
                //try downright
                try {
                    byte[] downRight = original[ROW_INDEX + 1][COL_INDEX + 1];
                    byte[] delta = arrays.deltaBytes(current, downRight);
                    new_image[2 * ROW_INDEX + 1][2 * COL_INDEX + 1] = delta;
                } catch (ArrayIndexOutOfBoundsException aioobe) {

                }
            }
        }
        return new_image;
    }

    public static int[][] toGreyscale(byte[][][] inputImage) {
        return toGreyscale(inputImage, RPERC, GPERC, BPERC);
    }

    public static int[][] toGreyscale(byte[][][] inputImage, double r_perc, double g_perc, double b_perc) {
        return applyThreshold(inputImage, r_perc, g_perc, b_perc, 0.0, false);
    }

    public static byte[][][] toGreyscale_b(byte[][][] inputImage) {
        return intToRGB(toGreyscale(inputImage, RPERC, GPERC, BPERC));
    }

    public static byte[][][] applyThreshold_b(byte[][][] inputImage, double d_threshold) {
        return intToRGB(applyThreshold(inputImage, RPERC, GPERC, BPERC, d_threshold, true));
    }

    public static int[][] applyThreshold(byte[][][] inputImage, double d_threshold) {
        return applyThreshold(inputImage, RPERC, GPERC, BPERC, d_threshold, true);
    }

    public static int[][] applyThreshold(byte[][][] inputImage, double r_perc, double g_perc, double b_perc, double d_threshold, boolean apply_threshold) {
        int[][] retImage = new int[inputImage.length][inputImage[0].length];

        double threshold = (int) (r_perc * d_threshold * 255 + g_perc * d_threshold * 255 + b_perc * d_threshold * 255);

        System.out.println("threshold:" + threshold);
        for (int i = 0; i < inputImage.length; i++) {
            for (int j = 0; j < inputImage[0].length; j++) {
                int r = inputImage[i][j][0], g = inputImage[i][j][1], b = inputImage[i][j][2];
                if(r < 0){
                    r+=256;
                }
                if(g < 0){
                    g+=256;
                }
                if(b < 0){
                    b+=256;
                }
                double GREY_d = (r_perc * r + g_perc * g + b_perc * b);
                int GREY = (int) GREY_d;
                if (GREY > 256) {
                    GREY -= 256;
                } else if (GREY < 0) {
                    GREY += 256;
                }
                if (apply_threshold) {
                    retImage[i][j] = rgbaToINT((GREY >= threshold ? WHITE_b : BLACK_b));
                } else {
                    r = GREY;
                    g = GREY;
                    b = GREY;
                    byte[] greyArray = new byte[]{(byte) r, (byte) g, (byte) b};
                    retImage[i][j] = rgbaToINT(greyArray);
                }
            }
        }
        return retImage;
    }

    public static char[][] applySlant(int[][] greyImage) {
        String ramp = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
        char[][] retImage = new char[greyImage.length][greyImage[0].length];
        for (int i = 0; i < greyImage.length; i++) {
            for (int j = 0; j < greyImage[0].length; j++) {
                int val = greyImage[i][j];
                double normalized = 0.5 - val / 255.0;
                //System.out.println("val:" + val + "\t normalized:" + normalized);
                retImage[i][j] = ramp.charAt((int) (normalized * ramp.length()));
            }
        }
        return retImage;
    }

    public static List<byte[][][]> loadGif(String pathName) throws IOException {
        File file = new File(pathName);
        BufferedImage img = ImageIO.read(file);
        int width = img.getWidth(), height = img.getHeight();
        System.out.printf("GIF Loaded. width=%s \theight=%s\n", width, height);
        System.out.println("Sources:" + img.getSources());
        GifDecoder gd = new GifDecoder();
        gd.read(pathName);
        int frames = gd.getFrameCount();
        System.out.println("Frames:" + frames);
        BufferedImage[] images = new BufferedImage[frames];
        for (int frame_index = 0; frame_index < frames; frame_index++) {
            images[frame_index] = gd.getFrame(frame_index);
        }
        List<byte[][][]> retList = new ArrayList();
        for(BufferedImage frame : images){
            retList.add(bufferedImageToBytes(frame, BufferedImage.TYPE_INT_RGB));
        }
        return retList;
    }
    
    public static byte[][][] loadImageBytes(String pathName) throws IOException {
        String extension = getFileExtension(new File(pathName));
        switch (extension) {
            case "bmp":
                return loadImageToByteArray(pathName, BufferedImage.TYPE_INT_RGB);
            case "png":
                return loadImageToByteArray(pathName, BufferedImage.TYPE_INT_ARGB);
            default:
                System.out.printf("PATHNAME:%s \t EXTENSION:%s", pathName, extension);
                throw new IllegalArgumentException("Invalid path, must be of type BMP or PNG.");
        }
    }

    public static void saveImageBytes(byte[][][] pixels, String pathName) throws IOException {
        String extension = getFileExtension(new File(pathName));
        switch (extension) {
            case "bmp":
                saveImageFromByteArray(pixels, pathName, BufferedImage.TYPE_INT_RGB);
                break;
            case "png":
                saveImageFromByteArray(pixels, pathName, BufferedImage.TYPE_INT_ARGB);
                break;
            default:
                System.out.printf("PATHNAME:%s \t EXTENSION:%s\n", pathName, extension);
                throw new IllegalArgumentException("Invalid path, must be of type BMP or PNG.");
        }
    }

    public static byte[][][] bufferedImageToBytes(BufferedImage img, final int CODEC){
        int width = img.getWidth(), height = img.getHeight();
        BufferedImage newImage = new BufferedImage(
                width, height, CODEC);

        Graphics2D graphic = newImage.createGraphics();
        graphic.drawImage(img, 0, 0, null);
        graphic.dispose();

        byte[][][] retMatrix = new byte[height][width][CODEC];
        int pixel = Integer.MIN_VALUE;
        byte r, g, b, a;
        for (int ROW_INDEX = 0; ROW_INDEX < height; ROW_INDEX++) {
            for (int COL_INDEX = 0; COL_INDEX < width; COL_INDEX++) {
                pixel = img.getRGB(COL_INDEX, ROW_INDEX);
                // Bitmaps don't use ARGB model by default
                retMatrix[ROW_INDEX][COL_INDEX] = (CODEC == BufferedImage.TYPE_INT_RGB ? intToRGB(pixel) : intToRGBA(pixel));
            }
        }
        return retMatrix;
    }
    
    public static byte[][][] loadImageToByteArray(String pathName, final int CODEC) throws IOException {
        BufferedImage img = ImageIO.read(new File(pathName));
        byte[][][] retMatrix = bufferedImageToBytes(img, CODEC);
        if (DEBUG) {
            System.out.println("Read success".toUpperCase());
        }
        return retMatrix;
    }

    public static int[][] loadImageToIntArray(String pathName, final int CODEC) throws IOException {
        BufferedImage img = ImageIO.read(new File(pathName));
        int width = img.getWidth(), height = img.getHeight();
        BufferedImage newImage = new BufferedImage(
                width, height, CODEC);

        Graphics2D graphic = newImage.createGraphics();
        graphic.drawImage(img, 0, 0, null);
        graphic.dispose();

        int[][] retMatrix = new int[height][width];
        int pixel = Integer.MIN_VALUE;
        for (int ROW_INDEX = 0; ROW_INDEX < height; ROW_INDEX++) {
            for (int COL_INDEX = 0; COL_INDEX < width; COL_INDEX++) {
                retMatrix[ROW_INDEX][COL_INDEX] = img.getRGB(COL_INDEX, ROW_INDEX);
            }
        }
        if (DEBUG) {
            System.out.println("Read success".toUpperCase());
        }
        return retMatrix;
    }

    public static void saveImageFromByteArray(byte[][][] pixels, String pathName, final int CODEC) throws IOException {
        int height = pixels.length, width = pixels[0].length;
        BufferedImage bi = new BufferedImage(width, height, CODEC);
        for (int ROW_INDEX = 0; ROW_INDEX < height; ROW_INDEX++) {
            for (int COL_INDEX = 0; COL_INDEX < width; COL_INDEX++) {
                bi.setRGB(COL_INDEX, ROW_INDEX, rgbaToINT(pixels[ROW_INDEX][COL_INDEX]));
            }
        }
        File file = new File(pathName);
        String extension = getFileExtension(file);
        ImageIO.write(bi, extension.toUpperCase(), file);
        if (DEBUG) {
            System.out.println("Write success".toUpperCase());
        }
    }

    public static void saveImageFromIntArray(int[][] pixels, String pathName, final int CODEC) throws IOException {
        int height = pixels.length, width = pixels[0].length;
        BufferedImage bi = new BufferedImage(width, height, CODEC);
        for (int ROW_INDEX = 0; ROW_INDEX < height; ROW_INDEX++) {
            for (int COL_INDEX = 0; COL_INDEX < width; COL_INDEX++) {
                bi.setRGB(COL_INDEX, ROW_INDEX, pixels[ROW_INDEX][COL_INDEX]);
            }
        }
        File file = new File(pathName);
        String extension = getFileExtension(file);
        ImageIO.write(bi, extension.toUpperCase(), file);
        if (DEBUG) {
            System.out.println("Write success".toUpperCase());
        }
    }

    public static String getFileExtension(File file) {
        String fileExtension = "";
        String fileName = file.getName();
        if (fileName.contains(".") && fileName.lastIndexOf(".") != 0) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return fileExtension;
    }

    public static byte[][][] intToRGB(int[][] input) {
        byte[][][] retArray = new byte[input.length][input[0].length][];
        for (int i = 0; i < input.length; i++) {
            retArray[i] = intToRGB(input[i]);
        }
        return retArray;
    }

    public static byte[][] intToRGB(int[] input) {
        byte[][] retArray = new byte[input.length][];
        for (int i = 0; i < input.length; i++) {
            retArray[i] = intToRGB(input[i]);
        }
        return retArray;
    }

    public static byte[] intToRGB(final int input) {
        return new byte[]{/*r*/(byte) ((input >> 16) & 0x000000FF), /*g*/ (byte) ((input >> 8) & 0x000000FF), /*b*/ (byte) (input & 0x000000FF)};
    }

    public static byte[] intToRGBA(final int input) {
        return new byte[]{/*r*/(byte) ((input >> 16) & 0x000000FF), /*g*/ (byte) ((input >> 8) & 0x000000FF), /*b*/ (byte) (input & 0x000000FF), /*a*/ (byte) ((input << 24) & 0x000000FF)};
    }

    public static int rgbaToINT(byte[] data) {
        return (((byte) (data.length == 4 ? data[3] : 0) << 24) + ((byte) data[0] << 16) + ((byte) data[1] << 8) + ((byte) data[2]));
    }

    public static int[] rgbaToINT(byte[][] data) {
        int[] retArray = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            retArray[i] = rgbaToINT(data[i]);
        }
        return retArray;
    }

    public static int[][] rgbaToINT(byte[][][] data) {
        int[][] retArray = new int[data.length][data[0].length];
        for (int i = 0; i < data.length; i++) {
            retArray[i] = rgbaToINT(data[i]);
        }
        return retArray;
    }

}
