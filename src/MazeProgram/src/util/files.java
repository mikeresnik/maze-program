package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public final class files {

    private files() {

    }

    public static void saveFile(char[][] input, String fileLocation) throws FileNotFoundException {
        File file = new File(fileLocation);
        PrintWriter pw = new PrintWriter(file);
        for (char[] row : input) {
            for (char elem : row) {
                pw.print(elem);
            }
            pw.println();
        }
        pw.close();
    }
}
